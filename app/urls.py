from django.urls import path,include
from .views import *

urlpatterns = [
    path("index/",show),
    path("add/", addStudent),
    path('edit/<int:id>', edit),
    path('update/<int:id>',update),
    path('delete/<int:id>',destroy)  
]
