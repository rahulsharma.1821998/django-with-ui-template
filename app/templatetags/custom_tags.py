# custom_tags.py
from django import template
register = template.Library()

#register your filter
@register.filter
def newString(value):
    return value.upper()