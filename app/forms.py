from django import forms 
from django.core.exceptions import ValidationError 
from .models import Student 

 
class StudentForm(forms.ModelForm):  
    class Meta:  
        model = Student  
        fields = "__all__"  
        
    def clean_password(self):
        value = self.cleaned_data.get('password')
        if len(value) < 5 or len(value) > 15:
            raise ValidationError("Password length should be between 5 and 15 characters.")
        return value
    